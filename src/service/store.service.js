import api from "../../src/utils/api";
import { authHeader } from "../utils/authHeader";

export const fetchStore = async () => {
  let response = await api.get("store", { headers: authHeader() })
  return response.data.data;
};

export const getStoreByPaging = async (page) => {
    const result = await api.get(`store/paging?page=${page}&limit=5`, {
      headers: authHeader()});
    return result.data;
};
export const deleteStore=async(id)=>{
  let result=await api.delete('/store/delete/'+id, {
    headers: authHeader(),
  });
  return result.data.message
}

// export const fetchPost = async (id) => {
//   let response = await api.get("posts" + id);
//   return response.data.data;
// };
export const createStore = async (post) => {
  let response = await api.post("store/create", post, {
    headers: authHeader(),
  });
  console.log("store:", response.data);
  return response.data;
};

export const updateStore = async (id, post) => {
  try {
    const result = await api.put(`/store/${id}`, post, {
      headers: authHeader(),
    });
    console.log("updateStoreById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("updateStoreById Error:", error);
  }
};

export const fetchStoreById = async (id) => {
  try {
    const result = await api.get(`/store/${id}/view`, {
      headers: authHeader(),
    });
    console.log("fetchStoreById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetchStoreById error:", error);
  }
};
