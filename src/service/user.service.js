import api from "../utils/api";
import { authHeader } from "../utils/authHeader";

export const fetchUser = async () => {
  let response = await api.get("users");
  return response.data.data;
};

export const deleteUser = async (id) => {
  let response = await api.delete("users/delete/" + id, {
    headers: authHeader(),
  });
  return response.data.message;
};
export const updateUserById = async (id, user) => {
  try {
    const result = await api.put(`/users/${id}`, user, {
      headers: authHeader(),
    });
    console.log("updateUserById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("updateUserById Error:", error);
  }
};

export const fetchUserbyID = async (id) => {
  let response = await api.get(`users/view/${id}`, { headers: authHeader() });
  return response.data.data;
};

export const updateUser = async (id, newUser) => {
  try {
    let response = await api.put(`users/${id}`, newUser, {
      headers: authHeader(),
    });
    return response.data.success;
  } catch (error) {
    console.log("update errro : ", error);
  }
};

export const uploadImage = async (profile) => {
  try {
    const fd = new FormData();
    fd.append("file", profile);
    const result = await api.post("files/upload", fd);
    console.log("uploadImage:", result.data.data.url);
    return result.data.data.url;
  } catch (error) {
    console.log("uploadImage Error:", error);
  }
};
export const createUser = async (user) => {
  let response = await api.post("users/create", user, {
    headers: authHeader(),
  });
  console.log("CREATE USER:", response.data);
  return response.data.message;
};

export const fetchUserById = async (id) => {
  try {
    const result = await api.get(`/users/view/${id}`, {
      headers: authHeader(),
    });
    console.log("fetchUserById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetchUserById error:", error);
  }
};

export const fetchUserByPage = async (page) => {
  try {
    const result = await api.get(`/users/filter?page=${page}&limit=5`, {
      headers: authHeader(),
    });
    return result.data;
  } catch (error) {
    console.log("fetchUserById error:", error);
  }
};

export const filterUser = async (firstName, lastName, phoneNumber) => {
  try {
    const result = await api.get(
      `/users/filter?name=${firstName || lastName || phoneNumber}&limit=10`,
      {
        headers: authHeader(),
      }
    );
    return result.data.data;
  } catch (error) {
    console.log("fetchUserById error:", error);
  }
};
