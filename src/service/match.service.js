import api from "../../src/utils/api";
import { authHeader } from "../utils/authHeader";

export const fetchMatchById = async (id) => {
    try {
      const result = await api.get(`/match/${id}/view`, {
        headers: authHeader(),
      });
      return result.data.data;
    } catch (error) {
      console.log("fetchmatchbyid error:", error);
    }
  };

  export const deleteMatch=async(id)=>{
    let result=await api.delete('/match/delete/'+id, {
      headers: authHeader(),
    });
    return result.data.message
  }

  export const getMatchByPaging = async (page) => {
    const result = await api.get(`match/paging?page=${page}&limit=5`, {
      headers: authHeader()});
    return result.data;
};

export const getMatch = async () => {
  let response = await api.get("match", { headers: authHeader() })
  return response.data.data;
};

export const createMatch = async (match) => {
    let response = await api.post("match/create", match, {
      headers: authHeader(),
    });
    console.log("match:", response.data);
    return response.data;
  };

  export const updateMatch = async (id, match) => {
    try {
      const result = await api.put(`/match/${id}`, match, {
        headers: authHeader(),
      });
      return result.data.data;
    } catch (error) {
      console.log("updatePostById Error:", error);
    }
  };