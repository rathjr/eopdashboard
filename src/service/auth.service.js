import api from "../utils/api"
import StringCrypto from 'string-crypto';
const {
    encryptString
  } = new StringCrypto();
export const handleLogin = async (phoneNumber, password) => {
    let response = await api.post('auth/login', { phoneNumber, password })
    if (response.data.success) {
        let user = response.data.data
        let encryptToken = encryptString(user.token,process.env.REACT_APP_SECRET)
        user.token = encryptToken
        localStorage.setItem("user",JSON.stringify(user))
    }
    return response
}
export const handleRegister = async (name,phoneNumber, password,roles) => {
    let response = await api.post('auth/register', {name,phoneNumber, password,roles })
    console.log(response);
    return response
}
