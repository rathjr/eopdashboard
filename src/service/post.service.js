import api from "../../src/utils/api";
import { authHeader } from "../utils/authHeader";

export const fetchPost = async () => {
  let response = await api.get("posts");
  return response.data.data;
};
export const fetchPlayer = async () => {
  let response = await api.get("player", { headers: authHeader() })
  return response.data.data;
};

export const getPlayerByPaging = async (page) => {
    const result = await api.get(`player/paging?page=${page}&limit=5`, {
      headers: authHeader()});
    return result.data;
};

export const deletePost = async (id) => {
  let response = await api.delete("posts/delete/" + id, {
    headers: authHeader(),
  });
  return response.data.success;
};

export const deletePlayer=async(id)=>{
  let result=await api.delete('/player/delete/'+id, {
    headers: authHeader(),
  });
  return result.data.message
}

export const handlePostData = async (title,description,serviceID,image) => {
  let response = await api.post('posts/create', { title, description,serviceID,image }, { headers: authHeader() })
  return response.data.success
}

// export const fetchPost = async (id) => {
//   let response = await api.get("posts" + id);
//   return response.data.data;
// };
export const createPlayer = async (post) => {
  let response = await api.post("player/create", post, {
    headers: authHeader(),
  });
  console.log("player:", response.data);
  return response.data;
};

export const updatePlayer = async (id, post) => {
  try {
    const result = await api.put(`/player/${id}`, post, {
      headers: authHeader(),
    });
    console.log("updatePostById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("updatePostById Error:", error);
  }
};

export const fetchPlayerById = async (id) => {
  try {
    const result = await api.get(`/player/${id}/view`, {
      headers: authHeader(),
    });
    console.log("fetchPostById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetchPostById error:", error);
  }
};

export const fetchPostBySId = async (serviceid) => {
  try {
    const result = await api.get(`/posts/${serviceid}/findpostbyserviceid`, {
      headers: authHeader(),
    });
    return result.data.data;
  } catch (error) {
    console.log("fetchServiceById error:", error);
  }
}

export const getPostByPaging = async (serviceid,page) => {
  try {
    const result = await api.get(`/posts/${serviceid}/findpostbyserviceid/paging?page=${page}&limit=3`, {
      headers: authHeader(),
    });
    return result.data
  } catch (error) {
    console.log("fetchServiceById error:", error);
  }
}
