import api from "../../src/utils/api";
export const uploadImage = async (image) => {
  try {
    const fd = new FormData();
    fd.append("file", image);
    const result = await api.post("/files/upload", fd);
    console.log("uploadImage:", result.data.data.url);
    return result.data.data.url;
  } catch (error) {
    console.log("uploadImage Error:", error);
  }
};
