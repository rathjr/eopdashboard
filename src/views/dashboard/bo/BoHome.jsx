/**********************************************************************
 * Development By Group: ServeNak
 * Description: business owner homepage
 **********************************************************************/

import { Table, Form } from "react-bootstrap";
import { Card, Button, Row, Col, Carousel, Container, Image } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { useDispatch, useSelector } from "react-redux";
import { getMatch } from "../../../service/match.service";
import { eq } from "lodash";
let user = JSON.parse(localStorage.getItem("user"));

const BoHome = () => {
  const [matchs, setMatchs] = useState([])
  const [win, setWin] = useState([])
  const [lose, setLose] = useState([])
  const [equal, setEqual] = useState([])
  useEffect(() => {
    const getMatchs = async () => {
        let result = await getMatch()
        setMatchs(result)
        setWin(result.filter(result => result.lresult==='win'))
        setLose(result.filter(result => result.lresult==='lose'))
        setEqual(result.filter(result => result.lresult==='equal'))
    }
  getMatchs()
},[])
console.log(matchs);
  const dispatch = useDispatch();
  // const viewService = useSelector((state) => state.serviceReducer.viewService);
  const [serviceID, setServiceiD] = useState("");

  // const defaultvalue = viewService[0].id;
  // const serviceID = service == null ? defaultvalue : service;
  // console.log("serviceID", viewService.get()[0]);

  const Loading = () => (
    <div className="text-center">
      <span
        className="spinner-border text-danger"
        style={{ width: "10rem", height: "10rem", marginTop: "70px" }}
        role="status"
      >
        Loading
      </span>
    </div>
  );
  // const meta = useSelector((state) => state.reviewReducer.meta);
  // const reviewsPaging = useSelector(
  //   (state) => state.reviewReducer.reviewsPaging
  // );
  const [isLoading, setIsLoading] = useState(true);
  const [isLoading2, setIsLoading2] = useState(true);
  // const onPageChange = ({ selected }) => {
  //   dispatch(fetchReviewByServicePaging(177, selected + 1)).then(() => {
  //     setIsLoading2(false);
  //   });
  // };
  // useEffect(() => {
  //   dispatch(fetchServiceByUID(user.id));
  //   dispatch(fetchReviewByServicePaging(177, 1)).then(() => {
  //     setIsLoading(false);
  //   });
  // }, []);

console.log('sid',serviceID);
  return (
    <div >
      <div className="content-wrapper">
        {/* Content Header (Page header) */}
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1
                  className="m-0"
                  style={{ fontSize: "40px", fontWeight: "bold"}}
                >
                  Match Report
                </h1>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="container-fluid" style={{marginTop:"10px"}}>
            {/* Small boxes (Stat box) */}
            <div className="row">
              <div className="col-lg-6 col-6">
                {/* small box */}
                <div className="small-box ">
                  <div className="inner">
                    <h5>All Matchs</h5>
                    <h1
                      style={{
                        fontSize: "90px",
                        fontWeight: "bold",
                        color: "#DAA520",
                      }}
                    >
                     {matchs.length}
                    </h1>
                  </div>
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-6 col-6">
                {/* small box */}
                <div className="small-box">
                  <div className="inner">
                    <h5>Win</h5>
                    <h1
                      style={{
                        fontSize: "90px",
                        fontWeight: "bold",
                        color: "#DAA520",
                      }}
                    >
                      {win.length}
                    </h1>
                  </div>
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-6 col-6" style={{marginTop:"15px"}}>
                {/* small box */}
                <div className="small-box">
                  <div className="inner">
                    <h5>Lose</h5>
                    <h1
                      style={{
                        fontSize: "90px",
                        fontWeight: "bold",
                        color: "#DAA520",
                      }}
                    >
                       {lose.length}
                    </h1>
                  </div>
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-6 col-6" style={{marginTop:"15px"}}>
                {/* small box */}
                <div className="small-box">
                  <div className="inner">
                    <h5>Equals</h5>
                    <h1
                      style={{
                        fontSize: "90px",
                        fontWeight: "bold",
                        color: "#DAA520",
                      }}
                    >
                       {equal.length}
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
   
      </div>
    </div>
  );
};

export default BoHome;
