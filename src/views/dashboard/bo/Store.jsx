/**********************************************************************
 * Development By Group: ServeNak
 * Description: Form for business owner post their services
 **********************************************************************/

 import { Form, Button, FormControl, Table, Pagination } from "react-bootstrap";
 import React, { useEffect, useState } from "react";
 import { useDispatch, useSelector } from "react-redux";
 import { bindActionCreators } from "redux";
 import { uploadImage } from "../../../service/uploadImg.service";
 import { updatePostById, fetchPostById, fetchPlayerById, createPlayer, updatePlayer, fetchPlayer, deletePlayer, getPlayerByPaging } from "../../../service/post.service";
 import { useLocation } from "react-router";
 import Swal from "sweetalert2";
 import ReactPaginate from "react-paginate";
 import "../../../assets/Style/carousel.css";
 import query from "query-string";
 import { useHistory } from "react-router";
 import {
   onDeletePost,
   fetchAllPosts,
 } from "./../../../Redux/actions/postAction";
import { set } from "lodash";
import { createStore, deleteStore, fetchStoreById, getStoreByPaging, updateStore } from "../../../service/store.service";
 
 let user = JSON.parse(localStorage.getItem("user"));
 const Store = () => {

  const btnvalidate = () => {
    if (name !== "" &&  price!== "" && description !== "" ) {
      return false  
    } else 
        return true 
}
   const posts = useSelector((state) => state.postReducer.posts);
   const dispatch = useDispatch();
   const onFetch = bindActionCreators(fetchAllPosts, dispatch);
   const [dateOfMatch, setDateOfMatch] = useState("");
   const [stores, setStores] = useState([])
   const [meta, setMeta] = useState({})
   const [name, setName] = useState("");
   const [price, setPrice] = useState("");
   const [images, setImages] = useState("");
   const [description, setDescription] = useState("");
   const [browsedImage, setBrowsedImage] = useState("");
   const [isPost, setIsPost] = useState(false);
   const placeholder = "../images/defaultphoto.jpg";
   const history = useHistory();
   const { search } = useLocation();
   let { id } = query.parse(search);
   console.log('dateofmatch',dateOfMatch);
   const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span
      className="spinner-border spinner-border-sm mb-1 ml-2"
      role="status"
      aria-hidden="tru"
    ></span>
  );
   useEffect(async () => {
     if (search == "") {
       setName("");
       setPrice("");
       setDescription("");
       setBrowsedImage("");
     } else {
       const result = await fetchStoreById(id);
       setName(result.name);
       setPrice(result.price);
       setDescription(result.description);
       setBrowsedImage(result.images);
     }
   }, [search]);

   const onDelete=(id)=>{
    deleteStore(id).then((success)=>{
      if (success) {
        setIsPost(false);
        Swal.fire({
          icon: "success",
          confirmButtonText: "OK",
        }).then((result) => {
          if (result.isConfirmed) {
            let tmp = stores.filter(item=>{
              return item._id!==id
      })
      setStores(tmp)
          }
        });
      }
    });
}

   useEffect(() => {
    const getStorePaging = async () => {
        let result = await getStoreByPaging(1)
        setStores(result.data)
        console.log("all match",stores);
    }
    const getMeta = async () => {
      let result = await getStoreByPaging(1)
      setMeta(result.metadata)
  }
  getStorePaging()
    getMeta()
},[])
const onPageChange= async ({selected})=>{
  let result = await getStoreByPaging(selected+1)
  setStores(result.data)
}
console.log('mymeta',meta);
   //Browse image from and add to image tag
   function onBrowsedImage(e) {
     setImages(e.target.files[0]);
     setBrowsedImage(URL.createObjectURL(e.target.files[0]));
   }
   console.log("search", search);
   //Add/Update  to api with image
   async function onSave() {
     setIsPost(true)
     if (search === "") {
       const url = images && (await uploadImage(images));
       console.log(url);
       const store = {
         name,
         price,
         description,
         images: url ? url : placeholder,
       };
       console.log(name,price,description,images);
       await createStore(store).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              setName("");
              setPrice("");
              setDescription("");     
              setBrowsedImage("");
              history.push("/bo/store");
            }
          });
        }
      });
     } else {
       const url = images && (await uploadImage(images));
       const match = {
        name,
        price,
        description,
        images: url ? url : browsedImage,
      };
       await updateStore(id, match).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo/store");
            }
          });
        }
      });
    
     }
   }
 
   return (
     <div>
       <div className="content-wrapper">
         <section className="content h-100">
           <div className="container-fluid">
             <div className="row mb-2">
             <div className="col-3">
                 <Form>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>Name</Form.Label>
                     <Form.Control
                       value={name}
                       onChange={(e) => setName(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>Price</Form.Label>
                     <Form.Control
                       value={price}
                       onChange={(e) => setPrice(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 </Form>
                 <div style={{ float: "right", marginBottom: "20px" }}>
                   <Button
                    disabled={btnvalidate()}
                     onClick={onSave}
                     className="ml-4 mt-3"
                     style={{
                       width: "100px",
                       border: "0",
                       fontWeight: "bold",
                       backgroundColor: "#DCDCDC",
                       color: "black",
                     }}
                   >
                     {search ? "Update" : "Create"}
                     {isPost ? Loading() : console.log("hello")}
                   </Button>{" "}
                 </div>
               </div>
               <div className="col-4">   
               <Form>
               <Form.Group className="mb-3" controlId="title">
                     <Form.Label>Description</Form.Label>
                     <Form.Control
                       value={description}
                       onChange={(e) => setDescription(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   
                 </Form>              
                 


               </div>
               
              
              
               <div className="col-4 p-0">
                 <div style={{ width: "600px" }}>
                   <label htmlFor="myfile">
                     <img
                       style={{ width: "400px" }}
                       src={browsedImage ? browsedImage : placeholder}
                     />
                   </label>
                 </div>
                 <input
                   onChange={onBrowsedImage}
                   id="myfile"
                   type="file"
                   style={{ display: "none" }}
                 />
               </div>
             </div>
             {/* search and table */}
             {/* <div className="row mb-4 ml-2">
               <Form className="d-flex">
                 <FormControl
                   type="search"
                   className="shadow p-1"
                   style={{ border: "0", width: "350px" }}
                   placeholder="ស្វែងរក"
                   aria-label="Search"
                 />
                 <Button
                   style={{
                     border: "red",
                     backgroundColor: "#E00000",
                     marginLeft: "-3px",
                     borderRadius: "0px 5px 5px 0px",
                   }}
                   variant="outline-success"
                 >
                   <i class="fas fa-search" style={{ color: "white" }}></i>
                 </Button>
               </Form>
             </div> */}
             <div className="row mb-2 ml-2">
               <Table striped bordered hover>
                 <thead>
                   <tr>
                     <th>#</th>
                     <th>Name</th>
                     <th>Price</th>
                     <th>Images</th>
                     <th>Description</th>
                   </tr>
                 </thead>
                 <tbody>
                   {stores.map((item, index) => (
                     <tr key={index}>
                       <td className="align-middle">{item.id}</td>
                       <td className="align-middle">{item.name}</td>
                       <td className="align-middle">{item.price}</td>
                       <td className="align-middle">
                         <img style={{ width: "100px" }} src={item.images} />
                       </td>
                       <td className="align-middle">{item.description}​</td>
                       <td>
                         <ul
                           style={{
                             margin: "0px",
                             padding: "0px",
                           }}
                         >
                           <li
                             onClick={() =>
                               history.push(`store?id=${item.id}`)
                             }
                             className="btn btn-edit btn-edit-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#28a745",
                             }}
                           >
                             <i
                               class="fas fa-pen-nib"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                           <li
                           onClick={()=>onDelete(item.id)}
                            //  onClick={() =>
                            //    onDelete(item.id).then((success) => {
                            //      console.log("success", success);
                            //      if (success) {
                            //        Swal.fire({
                            //          text: "អ្នកបានលុបដោយជោគជ័យ",
                            //          icon: "success",
                            //          confirmButtonText: "OK",
                            //        }).then((result) => {
                            //          if (result.isConfirmed) {
                            //            history.push("/bo/post-service");
                            //          }
                            //        });
                            //      }
                            //    })
                            //  }
                             className="btn btn-delete btn-delete-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#dc3545",
                             }}
                           >
                             <i
                               class="fas fa-trash"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                         </ul>
                       </td>
                     </tr>
                   ))}
                 </tbody>
               </Table>
             </div>
           </div>
         </section>
         <div style={{marginTop:"5px"}}>
           <ReactPaginate
 pageCount={meta.totalPages}
 onPageChange={onPageChange}
 containerClassName={"paginationBttns"}
 activeClassName={"paginationActive"}
  pageLinkClassName={"paginationActive"}
 disabledClassName={"paginationDisabled"}
       />
          </div>
       </div>
     </div>
   );
 };
 export default Store;
 