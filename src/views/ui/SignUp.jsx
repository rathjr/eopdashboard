/**********************************************************************
 * Development By Group: ServeNak
 * Description: SignIn Page
 **********************************************************************/
import React, { useState } from "react";
import { Form, Button, Container, Row, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useParams } from "react-router";
import { handleLogin, handleRegister } from "../../service/auth.service";
import { useHistory } from "react-router";
import { useEffect } from "react";
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import '../../assets/Style/App.css'
import Swal from 'sweetalert2'
import ReactDOM from 'react-dom';
import GoogleLogin from 'react-google-login';
import "../../assets/Style/carousel.css";
import ReactLoading from 'react-loading';
const SignUp = () => {
  const history = useHistory()
  let param = useParams()
  const [roles,setRoles] = useState(['admin']);
  const [name, setName] = useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [password, setPassword] = useState("")
  const [isSignup,setIsSignup] = useState(false)

  const btnvalidate = () => {
    if (name !== "" && phoneNumber !== "" && password !== "") {
      return false  
    } else 
        return true 
}
  useEffect(() => {
    console.log('log phonenumber',phoneNumber,isSignup);
  }, []);

  // const onLogin = (e) => {
  //   e.preventDefault()
  //   setIsSignup(true)
  //   handleLogin(phoneNumber, password).then(
  //     response => {
  //       if (response.data.success && response.data.data.roles[0] == 'ROLE_USER') {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/")
  //           }
  //         })
  //       } else if (response.data.success && response.data.data.roles[0] == 'ROLE_BO') {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/bo")
  //           }
  //         })
  //       } else {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/admin")
  //           }
  //         })
  //       }

  //       console.log('user', response.data.data);
  //     }
  //   )
  // }

  const handleGoogle = (res) => {
    console.log(res.profileObj);
    setName(res.profileObj.familyName)
    setPhoneNumber(res.profileObj.email)
    console.log(name,phoneNumber);
     handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        console.log('calling failed');
        handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo")
            }
          })
        } 

        console.log('user', response.data.data);
      }
    )
      }else{
        handleLogin(res.profileObj.email, password).then(
          response => {
            if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
              setIsSignup(false)
              Swal.fire({
                icon: 'success',
                confirmButtonText: 'OK'
              }).then((result) => {
                if (result.isConfirmed) {
                  history.push("/bo")
                }
              })
            } 
    
            console.log('user', response.data.data);
          }
        )
      }
    })
  };
 
  const onSignUp = (e) => {
    console.log('firstname',name,phoneNumber,password,roles,isSignup);
    e.preventDefault();
    setIsSignup(true)
    handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        Swal.fire({
          icon: 'success',
          confirmButtonText: 'OK'
        }).then((result) => {
          if (result.isConfirmed) {
            history.push("/sign-in")
          }
        })
      }else{
        setIsSignup(false)
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text:  response.data.message,
        })
      }
    })
  };
  const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span className="spinner-border spinner-border-sm mb-2 ml-2" role="status" aria-hidden="tru"></span>
  );
  return (
    <div>
      {/* Nabar */}
      <Navbar
        expand="lg"
        style={{ heigth: "50px", backgroundColor: "#0047AB", padding: "0" }}
      >
        <Container>
          <img
            style={{ heigth: "49px", width: "50px", margin: "auto" }}
            src="https://api.eop.digital/files/40f2fefd-75a8-41bb-af50-fc68e2f55bb8.png"
            alt=""
          ></img>
        </Container>
      </Navbar>
      <div style={{}}>
        <div
          className="container shadow p-3 mt-3 mb-2"
          style={{ borderRadius: "5px", backgroundColor: "#0047AB" }}
        >
          <h1
            style={{
              fontSize: "45px",
              fontWeight: "bold",
              marginTop: "20px",
              marginLeft: "140px",
              color:"white"
            }}
          >
            បង្កើតគណនី
          </h1>
          <Row>
            <div className="col-5">
              <Form>
                <Form.Group className="mb-3">
                  <Form.Label style={{color:"#DAA520"}}>លេខទូរស័ព្ទ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#DAA520"}}>ឈ្មោះ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setName(e.target.value)}
                  />
                
                  
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#DAA520"}}>ពាក្យសម្ងាត់</Form.Label>
                  <Form.Control
                    type="password"
                   
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                
                <Button
                disabled={btnvalidate()}
                onClick={onSignUp}
                  type="submit"
                  value="submit"
                  variant="primary"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#DAA520",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  បង្កើត
                  {
                  isSignup ? Loading() : console.log('hello')
                }

                </Button>
                <Button
                as={Link}
                to="/sign-in"
                  type="submit"
                  value="submit"
                  variant="primary"
                  style={{
                    margin: "20px 0px 10px 10px",
                    marginLeft : "25px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#DAA520",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  មានគណនី
                  
                </Button>
                
              </Form>
            </div>

            <div className="col-7">
              <img
                style={{ width: "99%" }}
                src="https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/274588486_5071615239581442_1486041313282791461_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeFPCKY9YfU2u1S23EE7HRdwq2m14MxWgCirabXgzFaAKDAyf0f27-KxzhOylSNvKAzeAs3889i7QL1h3Z8po9J_&_nc_ohc=9Y8TWZaEUloAX9c0ad9&_nc_ht=scontent-mrs2-2.xx&oh=00_AT8EGIRCnPvw1NYYKqil_woH-tFMGw8oAHdSI0zR09kdeA&oe=6249C249"
                alt=""
              ></img>
<div style={{display:"flex",justifyContent:"center"}}>
                {/* loading */}
                  </div>
            </div>
          </Row>
        </div>
      </div>
    </div>
  );
};
export default SignUp;
