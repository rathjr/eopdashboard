/**********************************************************************
 * Development By Group: ServeNak
 * Description: SignIn Page
 **********************************************************************/

import React, { useEffect } from "react";
import { Form, Button, Container, Row, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import { handleLogin, handleRegister } from "../../service/auth.service";
import { useState } from "react";
import Swal from 'sweetalert2'
import GoogleLogin from 'react-google-login';
import ReactLoading from 'react-loading';
const Login = () => {

  let user = JSON.parse(localStorage.getItem('user'))

  const [roles,setRoles] = useState([]);
  const [name, setName] =useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [password, setPassword] = useState("")
  const [isSignup, setIsSignup] = useState(false)
  const btnvalidate = () => {
    if (phoneNumber !== "" && password !== "") {
      return false
    } else
      return true
  }
  const history = useHistory()
  useEffect(() => {
    console.log('log phonenumber',phoneNumber,isSignup);
      roles[0]="admin"
  }, []);
  const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span className="spinner-border spinner-border-sm mb-2 ml-2" role="status" aria-hidden="tru"></span>
  );

  const handleGoogle = (res) => {
    console.log(res.profileObj);
    setName(res.profileObj.familyName)
    setPhoneNumber(res.profileObj.email)
    console.log(name,phoneNumber);
     handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        console.log('calling failed');
        handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/")
            }
          })
        }

      }
    )
      }else{
        handleLogin(res.profileObj.email, password).then(
          response => {
            if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
              setIsSignup(false)
              Swal.fire({
                icon: 'success',
                confirmButtonText: 'OK'
              }).then((result) => {
                if (result.isConfirmed) {
                  history.push("/admin")
                }
              })
            }
          }
        )
      }
    })
  };

  const onLogin = (e) => {
    e.preventDefault()
    setIsSignup(true)
    handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo")
            }
          })
        }

      }
    )
  }
  return (
    <div style={{ miniHeight: "100vh" }}>
      {/* Nabar */}

      <Navbar
        expand="lg"
        style={{ heigth: "50px", backgroundColor: "#0047AB", padding: "0" }}
      >
        <Container>
          <img
            style={{ heigth: "49px", width: "50px", margin: "auto" }}
            src="https://api.eop.digital/files/40f2fefd-75a8-41bb-af50-fc68e2f55bb8.png"
            alt=""
          ></img>
        </Container>
      </Navbar>

      <div>
        <div
          className="container shadow p-3 mt-3 mb-2"
          style={{ borderRadius: "5px", backgroundColor: "#0047AB",height:"530px"}}
        >
          <h1
            style={{
              fontSize: "45px",
              fontWeight: "bold",
              marginTop: "20px",
              marginLeft: "220px",
              color:"white"
            }}
          >
            ចូលគណនី
          </h1>

          <Row >
            <div className="col-5 mt-2 mx-4">
              <Form style={{ fontSize: "18px", fontWeight: "bold"}}>
                <Form.Group className="mb-3">
                  <Form.Label style={{color:"#DAA520"}}>លេខទូរស័ព្ទ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setPhoneNumber(e.target.value)}

                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#DAA520"}}>ពាក្យសម្ងាត់</Form.Label>
                  <Form.Control
                    type="password"
                    value={password} onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Button
                  disabled={btnvalidate()}
                  onClick={onLogin}
                  variant="primary"
                  type="submit"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#DAA520",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  ចូលគណនី
                  {
                  isSignup ? Loading() : console.log('hello')
                }
                </Button>
               
                <Button
                  // onClick={onLogin}
                  as={Link}
                  to="/sign-up"
                  variant="primary"
                  type="submit"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#DAA520",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  បង្កើតគណនី
                </Button>
                
              </Form>
            </div>

            <div className="col-6" >
              <img
                style={{ width: "105%",height:"100%" }}
                src="https://scontent.fpnh2-1.fna.fbcdn.net/v/t1.6435-9/120654482_3486513078091674_5414666245516869132_n.jpg?stp=dst-jpg_s960x960&_nc_cat=100&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeHvUUGbfnnAlpxai8748g5v72ooFg-jiKfvaigWD6OIpx01UXFmNdBz0Xn3kjOgAaeiQhMHXxo-bzwhqzLqgQ6C&_nc_ohc=KekLAVeV4gIAX8e9Ord&_nc_ht=scontent.fpnh2-1.fna&oh=00_AT-AVSCkZY1w6u1IHXl-LA5nnfrHx_nWh5_9dr8DxpHp8w&oe=6251E074"
              ></img>
            </div>
          </Row>
        
                
       
        </div>
      </div>
    </div>
  );
}
export default Login;