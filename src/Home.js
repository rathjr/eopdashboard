import React from "react";
import {Switch, Route } from "react-router-dom";
import SignUp from './components/views/signUp';
import Contact from "./components/views/contact";
import Repair from './components/views/repair';
import NewService from './components/views/newService';
import ServiceDetail from './components/views/ServiceDetail';
import aboutUs from './components/views/aboutUs';
import allService from "./components/views/allService";
import chooseRole from "./components/views/chooseRole";
import homePage from './components/views/homePage';
import install from './components/views/install';
import NotFound from './components/views/notFound';
import SignIn from './components/views/signIn';
import ResultSearch from "./components/views/resultSearch";


const Client = ({match}) => {
  return (
    <div>
       
        <Switch>
          <Route path={match.url} exact={true} component={homePage} />
          <Route path={`${match.url}/aboutus`} exact={true} component={aboutUs} />
          <Route path={`${match.url}/contact`} exact={true} component={Contact} />
          <Route path={`${match.url}/choose`} exact={true} component={chooseRole} />
          <Route path={`${match.url}/signup`} exact={true} component={SignUp} />
          <Route path={`${match.url}/signin`} exact={true} component={SignIn} />
          <Route path={`${match.url}/allservice`} exact={true} component={allService} />
          <Route path={`${match.url}/install`} exact={true} component={install} />
          <Route path={`${match.url}/repair`} exact={true} component={Repair} /> 
          <Route path={`${match.url}/Newservice`} exact={true} component={NewService} />
          <Route path={`${match.url}/viewdetail`} exact={true} component={ServiceDetail} /> 
          <Route path={`${match.url}/resultsearch`} exact={true} component={ResultSearch} />
          <Route path={`${match.url}/notfound`} exact={true} component={NotFound} />
        </Switch>
    </div>
  );
};
export default Client;
