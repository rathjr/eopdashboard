/**********************************************************************
 * Development By Group: ServeNak
 * Description: ui homepage
 **********************************************************************/

import React from 'react'
import Footer from '../../components/footer/footer.js';
import Menu from '../../components/header/menu.js'
import MyBody from './MyBody.jsx';

export default function HomePage() {
    return (
        <div style={{backgroundColor:"white"}}>
            <Menu />
            <MyBody />
            <Footer/>
        </div>
    )
}
