import { userType as typeserve } from "../actions/type";
import { userType } from "./../actions/type";
const initialState = {
  users: [],
  viewUser: {},
  usersPaging: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case userType.FETCH_USER:
      return { ...state, users: [...payload] };

    case typeserve.FETCH_USER_BY_ID:
      return { ...state, viewUser: payload };

    case typeserve.FETCH_USER_BY_PAGING:
      return {
        ...state,
        usersPaging: [...payload.data],
        meta: payload.metadata,
      };

    case typeserve.FILTER_USER:
      return { ...state, filter: [...payload] };
    case typeserve.DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== payload),
      };

    case userType.DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== payload),
      };
    default:
      return state;
  }
};
