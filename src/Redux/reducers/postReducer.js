import { postType } from "../actions/type";
const initialState = {
  posts: [],
  viewPost: [],
  postPaging: [],
  meta: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case postType.FETCH_POST:
      return { ...state, posts: [...payload] };
    case postType.DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== payload),
      };
    case postType.FETCH_POST_BY_PAGING:
      return {
        ...state,
        postPaging: [...payload.data],
        meta: payload.metadata,
      };

    case postType.FETCH_POST_BY_ID:
      return { ...state, viewPost: [...payload] };
    default:
      return state;
  }
};

// export default (state = initialState, { type, payload }) => {
//   switch (type) {
//     case typeserve.FETCH_CATEGORY:
//       return { ...state, categorys: [...payload] };
//     default:
//       return state;
//   }
// };
