import {
  deleteUser,
  fetchUser,
  fetchUserbyID,
  fetchUserByPage,
} from "../../service/user.service";
import { userType } from "./type";

export const fetchAllUsers = () => async (dp) => {
  let users = await fetchUser();
  dp({
    type: userType.FETCH_USER,
    payload: users,
  });
};

export const fetchUserbyPaging = (page) => async (dp) => {
  let usersPaging = await fetchUserByPage(page);
  dp({
    type: userType.FETCH_USER_BY_PAGING,
    payload: usersPaging,
  });
};

export const filterUser = (firstName, lastName, phoneNumber) => async (dp) => {
  let filter = await filterUser(firstName, lastName, phoneNumber);
  dp({
    type: userType.FILTER_USER,
    payload: filter,
  });
};

export const viewUserbyID = (id) => async (dp) => {
  let viewUser = await fetchUserbyID(id);
  dp({
    type: userType.FETCH_USER_BY_ID,
    payload: viewUser,
  });
};

export const onDeleteUser = (id) => async (dp) => {
  let message = await deleteUser(id);
  dp({
    type: userType.DELETE_USER,
    payload: id,
  });
  return Promise.resolve(message);
};
