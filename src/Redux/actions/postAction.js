import { deletePost, fetchPost, fetchPostBySId, getPostByPaging } from "../../service/post.service";
import { postType } from "./type";
  
  export const fetchAllPosts = () => async (dp) => {
    let posts = await fetchPost();
    dp({
      type: postType.FETCH_POST,
      payload: posts,
    });
  };

  export const getPostbySID = (serviceid) => async (dp) => {
    let viewPost = await fetchPostBySId(serviceid);
     dp({
        type: postType.FETCH_POST_BY_ID,
        payload: viewPost,
    });
}

export const fetchPostbyPaging = (serviceid,page) => async (dp) => {
  let postPaging = await getPostByPaging(serviceid,page);
   dp({
      type: postType.FETCH_POST_BY_PAGING,
      payload: postPaging,
  });
}

  

  
//   export const onDeleteReports = (id) => async (dp) => {
//     let message = await deleteReport(id);
//     dp({
//       type: reportType.DELETE_REPORT,
//       payload: id,
//     });
//     return Promise.resolve(message);
//   };
  
//   export const onPostService = (contentReport) => async (dp) => {
//     let postreports = await postReport("reports/create", contentReport);
//     dp({
//       type: reportType.POST_REPORT,
//       payload: postreports,
//     });
//     return Promise.resolve(postreports);
//   };

export const onDeletePost = (id) => async (dp) => {
  let message = await deletePost(id);
  dp({
    type: postType.DELETE_POST,
    payload: id,
  });
  return Promise.resolve(message);
};
