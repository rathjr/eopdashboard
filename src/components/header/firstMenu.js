import React from "react";
 import { Nav, Navbar, NavDropdown, Container, Button } from "react-bootstrap";
 import { Link } from "react-router-dom";
 import '../../assets/Style/App.css'

export default function firstMenu() {
   return (
     <div >
       <Navbar className="fixMenu" expand="lg" style={{fontSize:"22px",fontWeight:"bold",backgroundColor:"white", position:"fixed",width:"100%"}}>
         <Container>
           <img
               style={{ width: "50px",height:"50px" }}
               src="../images/servenaklogo.png"
               className="img-circle mr-2"
               alt="User"
           />
           <Navbar.Toggle aria-controls="basic-navbar-nav" />
           <Navbar.Collapse id="basic-navbar-nav">
             <Nav className="me-auto" >
               <Nav.Link as={Link} to="/">ទំព័រដើម</Nav.Link>
               <Nav.Link as={Link} to="/home/about-us">អំពីយើង</Nav.Link>
               <Nav.Link as={Link} to="/home/contact">ទំនាក់ទំនង</Nav.Link>
             </Nav>
           </Navbar.Collapse>
           <Navbar.Collapse id="basic-navbar-nav" className="d-flex justify-content-end">
                 <Button as={Link} to="/sign-in"  variant="danger" style={{fontSize:"18px",backgroundColor:"#e30000"}}>ចូលគណនី</Button>{' '}
           </Navbar.Collapse>
         </Container>
       </Navbar>
     </div>
   );
 }