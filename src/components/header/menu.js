/**********************************************************************
 * Development By Group: ServeNak
 * Description: main homepage navbar with image background
 **********************************************************************/

import React from "react";
import { Nav, Navbar, NavDropdown, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import '../../assets/Style/App.css'
export default function Menu() {
  return (
    <div>
      <Navbar
        className="fixMenu"
        expand="lg"
        style={{
          fontSize: "22px",
          fontWeight: "bold",
          backgroundColor: "white",
          position: "fixed",
          width: "100%",
        }}
      >
        <Container>
          <img
            style={{ width: "50px", height: "50px" }}
            src="../images/servenaklogo.png"
            className="img-circle mr-2"
            alt="User"
          />
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">
                ទំព័រដើម
              </Nav.Link>
              <Nav.Link as={Link} to="/home/about-us">
                អំពីយើង
              </Nav.Link>
              <Nav.Link as={Link} to="/home/contact">
                ទំនាក់ទំនង
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="d-flex justify-content-end"
          >
            <Nav>
              <li className="nav-item dropdown">
                <a className="nav-link p-0" data-toggle="dropdown" href="">
                  <img
                    style={{ width: "50px", margin: "0px" }}
                    src="https://img.icons8.com/color/48/000000/user-male-circle--v1.png"
                    className="img-circle"
                    alt="User Image"
                  />
                </a>
                <div
                  className="dropdown-menu dropdown-menu-lg dropdown-menu"
                  style={{
                    position: "absolute",
                    top: "50px",
                    left: "-50px",
                    maxWidth: "100px",
                  }}
                >
                  <div className="dropdown-divider" />
                  <Link to="/admin/admin-setting">
                    <a href="#" className="dropdown-item red-hv">
                      ចូលគណនី
                    </a>
                  </Link>
                  <Link to="/admin">
                    <a href="#" className="dropdown-item red-hv">
                      ចាកចេញ
                    </a>
                  </Link>
                </div>
              </li>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
