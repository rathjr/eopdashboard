/**********************************************************************
 * Development By Group: ServeNak
 * Description: admin dashboard navbar
 **********************************************************************/

import React from "react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { fetchUserById } from "../../service/user.service";
import { useDispatch, useSelector } from "react-redux";
import { viewUserbyID } from "../../Redux/actions/userAction";
let user = JSON.parse(localStorage.getItem("user"));
let removeItem = () => localStorage.removeItem("user");

const AdminHeader = () => {
  console.log("usercredental", user);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [profile, setProfile] = useState("");
  const [browsedImage, setBrowsedImage] = useState("");
  const placeholder = "../images/profile.jpg";

  useEffect(() => {
    fetchUserById(user.id).then((response) => {
      setFirstName(response.firstName);
      setLastName(response.lastName);
      setProfile(response.profile);
    });

    if (user !== null) {
      dispatch(viewUserbyID(user.id));
    }
  }, []);

  const viewUser = useSelector((state) => state.userReducer.viewUser);
  const dispatch = useDispatch();
  let removeItem = () => localStorage.removeItem("user");
  // useEffect(() => {
  //   if(user!==null){
  //   dispatch(viewUserbyID(user.id))
  //   }
  // }, []);
  console.log("viewuser : ", viewUser);

  return (
    <div>
      <nav
        className="main-header navbar navbar-expand"
        style={{ backgroundColor: "#e30000", color: "white" }}
      >
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" data-widget="pushmenu" role="button">
              <i style={{ color: "white" }} className="fas fa-bars" />
            </a>
          </li>
        </ul>
        <div
          className="d-flex justify-content-center"
          style={{ fontSize: "25px", fontFamily: "Poppins" }}
        >
          <img
            src="../images/servenaklogo.png"
            alt="logoimages"
            style={{ height: "40px", padding: "0px", alignItems: "center" }}
            className="img-circle"
          />
          {}
          <span className="ml-2">servenak</span>
        </div>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a
              className="nav-link"
              data-widget="fullscreen"
              href="#"
              role="button"
            >
              <i
                style={{ color: "white" }}
                className="fas fa-expand-arrows-alt"
              />
            </a>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link" data-toggle="dropdown" href="#">
              <i
                class="fas fa-bars"
                style={{ color: "white", fontSize: "20px" }}
              ></i>
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <div className="dropdown-divider" />
              <Link to="/admin/admin-setting">
                <a href="#" className="dropdown-item red-hv">
                  ការកំណត់
                </a>
              </Link>
              <Link to="/sign-in" onClick={removeItem}>
                <a href="#" className="dropdown-item red-hv">
                  ចាកចេញ
                </a>
              </Link>
            </div>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default AdminHeader;
