import React, { useEffect } from "react";
import { Nav, Navbar, NavDropdown, Container, Image } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "../../assets/Style/App.css";
import { viewUserbyID } from "../../Redux/actions/userAction";

// import { enableRipple } from '@syncfusion/ej2-base';
// enableRipple(true);

export const SecondMenu = () => {
  const viewUser = useSelector((state) => state.userReducer.viewUser);
  const dispatch = useDispatch();
  let user = JSON.parse(localStorage.getItem("user"));
  let removeItem = () => localStorage.removeItem("user");
  useEffect(() => {
    if (user !== null) {
      dispatch(viewUserbyID(user.id));
    }
  }, []);
  return (
    <div>
      <Navbar
        className="fixMenu"
        expand="lg"
        style={{
          fontSize: "22px",
          fontWeight: "bold",
          backgroundColor: "white",
          position: "fixed",
          width: "100%",
        }}
      >
        <Container>
          <img
            style={{ width: "50px", height: "50px" }}
            src="../images/servenaklogo.png"
            className="img-circle mr-2"
            alt="User"
          />
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="container-fluid">
              <Nav.Link as={Link} to="/">
                ទំព័រដើម
              </Nav.Link>
              <Nav.Link as={Link} to="/home/about-us">
                អំពីយើង
              </Nav.Link>
              <Nav.Link as={Link} to="/home/contact">
                ទំនាក់ទំនង
              </Nav.Link>
              <Nav.Link as={Link} to="" className="ml-auto">
              <Nav>
              <li className="nav-item dropdown">
                <a className="nav-link p-0" data-toggle="dropdown" href="">
                  {
                    viewUser.profile !== null ? <Image src={viewUser.profile} roundedCircle style={{ width: "45px", height: "45px", margin: "0px" }} /> : <Image src="data:image/svg+xml;base64,PHN2ZyB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgNTAgNTAiIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIG1lZXQiPjxtZXRhZGF0YT48cmRmOlJERj48Y2M6V29yaz48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlPkpkZW50aWNvbjwvZGM6dGl0bGU+PGRjOmNyZWF0b3I+PGNjOkFnZW50PjxkYzp0aXRsZT5EYW5pZWwgTWVzdGVyIFBpcnR0aWrDpHJ2aTwvZGM6dGl0bGU+PC9jYzpBZ2VudD48L2RjOmNyZWF0b3I+PGRjOnNvdXJjZT5odHRwczovL2dpdGh1Yi5jb20vZG1lc3Rlci9qZGVudGljb248L2RjOnNvdXJjZT48Y2M6bGljZW5zZSByZGY6cmVzb3VyY2U9Imh0dHBzOi8vZ2l0aHViLmNvbS9kbWVzdGVyL2pkZW50aWNvbi9ibG9iL21hc3Rlci9MSUNFTlNFIi8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxyZWN0IGZpbGw9InRyYW5zcGFyZW50IiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHg9IjAiIHk9IjAiLz48cGF0aCBmaWxsPSIjNTQ1NDU0IiBkPSJNMTMgMUwyNSAxTDI1IDEzWk0zNyAxTDM3IDEzTDI1IDEzWk0zNyA0OUwyNSA0OUwyNSAzN1pNMTMgNDlMMTMgMzdMMjUgMzdaTTEgMTNMMTMgMTNMMTMgMjVaTTQ5IDEzTDQ5IDI1TDM3IDI1Wk00OSAzN0wzNyAzN0wzNyAyNVpNMSAzN0wxIDI1TDEzIDI1WiIvPjxwYXRoIGZpbGw9IiNlOGU4ZTgiIGQ9Ik0xIDdMNyAxTDEzIDdMNyAxM1pNNDMgMUw0OSA3TDQzIDEzTDM3IDdaTTQ5IDQzTDQzIDQ5TDM3IDQzTDQzIDM3Wk03IDQ5TDEgNDNMNyAzN0wxMyA0M1oiLz48cGF0aCBmaWxsPSIjZDE5ODc1IiBkPSJNMTYgMTZMMjQgMTZMMjQgMjRMMTYgMjRaTTM0IDE2TDM0IDI0TDI2IDI0TDI2IDE2Wk0zNCAzNEwyNiAzNEwyNiAyNkwzNCAyNlpNMTYgMzRMMTYgMjZMMjQgMjZMMjQgMzRaIi8+PC9zdmc+" roundedCircle style={{ width: "45px", height: "45px", margin: "0px" }} />
                  }
                  <img
                    src="https://img.icons8.com/ios-glyphs/50/000000/menu-2.png"
                    style={{ width: "35px", margin: "0px" }}
                    className="red-hv"
                  />
                </a>
                <div
                  className="dropdown-menu dropdown-menu-md dropdown-menu"
                  style={{
                    position: "absolute",
                    top: "50px",
                    left: "-50px",
                    Width: "50px",
                  }}
                >
                  <div />
                  <Link to="/home/user-setting">
                    <a href="#" className="dropdown-item red-hv">
                      ការកំណត់
                    </a>
                  </Link>
                  <Link to="/home/favorite">
                    <a href="#" className="dropdown-item red-hv">
                      ចំណូលចិត្ត
                    </a>
                  </Link>
                  <Link to="/sign-in" onClick={removeItem}>
                    <a href="#" className="dropdown-item red-hv">
                      ចាកចេញ
                    </a>
                  </Link>
                </div>
              </li>
            </Nav>
         
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>;
