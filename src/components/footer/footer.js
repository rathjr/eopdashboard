/**********************************************************************
 * Development By Group: ServeNak
 * Description: footer for homepage (ui client)
 **********************************************************************/

import React from "react";
import { Link } from "react-router-dom";
import { Nav } from "react-bootstrap";

export default function Footer() {
  return (
    <div class="container" style={{ height: "250px", paddingTop: "40px" }}>
      <div class="row">
        <div class="col">
          <div style={{ marginTop: "-35px" }}>
            <Nav.Link as={Link} to="/">
              <img
                src="http://3.142.146.250:16000/files/39d799d8-c526-4bd3-9bca-3185a1410cc6.png"
                style={{ width: "70px", marginTop: "18px", marginLeft: "30px" }}
              ></img>
              <span
                style={{
                  fontSize: "30px",
                  marginLeft: "0px",
                  paddingLeft: "0px",
                  verticalAlign: "middle",
                  color: "black",
                }}
              >
                SERVENAK
              </span>
            </Nav.Link>
          </div>
          <p style={{ paddingLeft: "15px", marginTop: "-10px" }}>
            សេវាកម្មល្អ រហ័សទាន់ចិត្ដ​ <br /> និងមានទំនុកចិត្តខ្ពស់
          </p>
        </div>
        <div class="col-sm" style={{ marginLeft: "45px" }}>
          <Nav.Link
            style={{ paddingLeft: "0px" }}
            as={Link}
            to="/home/about-us"
          >
            <h4 style={{ color: "black" }}>អំពីយើង</h4>
          </Nav.Link>
          <ul
            style={{
              textDecoration: "none",
              listStyle: "none",
              padding: "0px",
              color: "black",
              //  marginLeft:"18px"
            }}
          >
            <li>
              <a href="#" style={{ color: "black" }}>
                ការណែនាំ
              </a>
            </li>
            <li>
              <a href="#" style={{ color: "black" }}>
                លក្ខខណ្ឌ និង គោលការណ៏
              </a>
            </li>
            <li>
              <a href="#" style={{ color: "black" }}>
                ទំនុកចិត្ត និង សុវត្ថិភាព
              </a>
            </li>
            <li>
              <a href="#" style={{ color: "black" }}>
                ក្រុមរបស់ពួកយើង
              </a>
            </li>
          </ul>
        </div>

        <div class="col-sm" style={{ marginLeft: "15px" }}>
          <Nav.Link
            style={{ color: "black", paddingLeft: "0px" }}
            as={Link}
            to="/home/contact"
          >
            <h4 style={{ color: "black" }}>ទំនាក់ទំនង</h4>{" "}
          </Nav.Link>
          <ul
            style={{
              textDecoration: "none",
              listStyle: "none",
              padding: "0px",
            }}
          >
            <li>លេខទូរស័ព្ទ: 023 333 333</li>
            <li>អុីម៉ែល: servenak@gmail.com</li>
            <li>បណ្ដាញសង្គម:</li>
            <li>
              <a
                target="_blank"
                href="https://www.facebook.com/profile.php?id=100005736404791"
              >
                <img
                  style={{ width: "35px" }}
                  src="https://img.icons8.com/color/48/000000/facebook.png"
                />
              </a>
              <a target="_blank" href="https://t.me/songchanmoni">
                {" "}
                <img
                  style={{ width: "35px" }}
                  src="https://img.icons8.com/color/48/000000/telegram-app--v4.png"
                />
              </a>
              <a>
                <img
                  style={{ width: "35px" }}
                  src="https://img.icons8.com/color/48/000000/line-me.png"
                />
              </a>
              <a
                target="_blank"
                href="https://www.instagram.com/song.chanmony.9/"
              >
                <img
                  style={{ width: "35px" }}
                  src="https://img.icons8.com/color/48/000000/instagram-new--v1.png"
                />
              </a>
            </li>
          </ul>
        </div>

        <div class="col-md" style={{ marginLeft: "15px" }}>
          <Nav.Link style={{ color: "black", paddingLeft: "0px" }}>
            <h4 style={{ color: "black" }}>អាស័យដ្ឋាន</h4>{" "}
          </Nav.Link>
          <p>
            ផ្ទះ លេខ ១២ ផ្លូវលេខ ៣២៣ <br />
            សង្កាត់ បឹងកក់ II ខណ្ឌ ទួលគោក <br />
            រាជធានី ភ្នំពេញ ព្រះរាជាណាចក្រកម្ពុជា
          </p>
        </div>
        
      </div>
    </div>
  );
}
