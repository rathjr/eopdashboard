/**********************************************************************
 * Development By Group: ServeNak
 * Description: admin dashboard sidebar
 **********************************************************************/

import React from "react";
import { Link } from "react-router-dom";
import "../../assets/Style/sidebar.css";
import { useEffect, useState } from "react";
import { fetchUserById } from "../../service/user.service";
import { Image } from "react-bootstrap";

let removeItem = () => localStorage.removeItem("user");
let user = JSON.parse(localStorage.getItem("user"));
console.log("usercredental", user);
const AdminSidebar = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [profile, setProfile] = useState("");

  useEffect(() => {
    fetchUserById(user.id).then((response) => {
      setFirstName(response.firstName);
      setLastName(response.lastName);
      setProfile(response.profile);
    });
  }, []);
  return (
    <div>
      <aside
        style={{ backgroundColor: "#EBEBEB", color: "black" }}
        className="main-sidebar elevation-4"
      >
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 d-flex">
            <div className="image">
              {profile !== null ? (
                <Image
                  src={profile}
                  roundedCircle
                  style={{ width: "45px", height: "45px", margin: "0px" }}
                />
              ) : (
                <Image
                  src="https://profiles.utdallas.edu/img/default.png"
                  roundedCircle
                  style={{ width: "45px", height: "45px", margin: "0px" }}
                />
              )}
            </div>
            <div
              className="info"
              style={{ fontSize: "25px", paddingBottom: "20px" }}
            >
              <Link to="/admin/admin-setting">
                <a href="#" className="d-block" style={{ color: "#6c757d" }}>
                  {firstName}
                </a>
              </Link>
            </div>
          </div>
          <nav className="mt-2" style={{ fontSize: "22px" }}>
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <li className="nav-item">
                <Link to="/admin">
                  <a href="#" className="nav-link">
                    <i className="nav-icon fas fa-home" />
                    <p>ទំព័រដើម</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/admin/manage-user">
                  <a href="" className="nav-link">
                    <i class="fas fa-user-alt nav-icon"></i>
                    <p>គ្រប់គ្រងអ្នកប្រើប្រាស់</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/admin/manage-service">
                  <a href="" className="nav-link">
                    <i class="fab fa-servicestack nav-icon " />
                    <p>គ្រប់គ្រងសេវាកម្ម</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/admin/create-category">
                  <a href="" className="nav-link">
                    <i className="fas fa-folder-plus nav-icon" />
                    <p>បង្កើតប្រភេទសេវាកម្ម</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/admin/manage-report">
                  <a href="" className="nav-link">
                    <i className="fas fa-info-circle nav-icon" />
                    <p>របាយការណ៏</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/sign-in" onClick={removeItem}>
                  <a href="pages/widgets.html" className="nav-link">
                    <i className="nav-icon fas fa-sign-out-alt" />
                    <p>ចាកចេញ</p>
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </aside>
    </div>
  );
};

export default AdminSidebar;
