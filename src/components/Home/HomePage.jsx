import React from 'react'
import Menu from './Menu'
import MyBody from './MyBody';
import Footer from './Footer';
import NavMenu from './Navbar';
import Body from './Body';


export default function HomePage() {
    return (
        <div>
            <NavMenu />
            <Body />
            <Footer />
        </div>
    )
}
