import React, { Link } from "react";
import { NavLink } from 'react-bootstrap'


export default function Footer() {
  return (
    <div style={{ marginTop: "50px" }}>
      <footer class="text-center text-lg-start bg-light text-muted">
        <section class="">
          <div class="container text-left text-md-start">
            <div class="row mt-3" style={{ marginTop: "50px" }}>
              <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                <div class="row" style={{ marginTop: "50px" }}>
                  <div class="col-3" style={{ marginRight: "5px" }}>
                    <div class="col-8">
                      <img
                        src="../images/servenaklogo.png"
                        style={{ width: "100px", height: "100px", marginLeft: "40px" }}
                      />
                      <div class="col-4">
                        <h6 style={{ fontFamily: "poppins", marginLeft: "40px", marginTop: "5px" }}>ServeNak</h6>
                      </div>
                    </div>
                  </div>
                  <div class="col-10" style={{}}>
                    <div >
                      <p style={{ fontFamily: "nokora", fontSize: "15px", lineHeight: "30px" }}>
                        <span style={{ fontFamily: "poppins-regular", fontSize: "13px", marginBottom: "5px" }}>Web Services </span>
                        នៅប្រទេសកម្ពុជា​​ ត្រូវបានបង្កើតឡើងដោយនិស្សិតជំនាន់ទី៩ នៃមជ្ឈមណ្ឌលកូរ៉េ​ សហ្វវែរ អេច អ ឌី
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div
                class="col-md-3  mx-auto"
                style={{ marginTop: "85px", textAlign: "center", lineHeight: "30px", paddingRight: "50px"}}
              >
                <h6
                  style={{ fontFamily: "nokora-bold", fontSize: "22px" }}
                  class="text-uppercase fw-bold"
                >
                  អំពីយើង
                </h6>
                <a
                  style={{ fontFamily: "nokora", fontSize: "17px", textDecoration: "none"}}
                  href="#!"
                  class="text-reset"
                >
                  ការណែនាំ
                </a>
                <br/>
                <a
                  style={{ fontFamily: "nokora", fontSize: "17px", textDecoration: "none"}}
                  href="#!"
                  class="text-reset"
                >
                  លក្ខខណ្ឌ​​ និង គោលការណ៏
                </a>
                <br/>
                <a
                  style={{ fontFamily: "nokora", fontSize: "17px", textDecoration: "none"}}
                  href="#!"
                  class="text-reset"
                >
                  ទំនុកចិត្ត និង សុវត្ថិភាព
                </a>
                <br/>
                <a
                  style={{ fontFamily: "nokora", fontSize: "17px", textDecoration: "none" }}
                  href="#!"
                  class="text-reset"
                >
                </a>
              </div>
              <div class="col-md-3 mx-auto mb-4" style={{ marginTop: "85px", textAlign: "center", paddingRight: "50px"}}>
                <h6 style={{ fontFamily: "nokora-bold", fontSize: "22px" }}>ទំនាក់ទំនង</h6>
                <p style={{ fontFamily: "nokora", fontSize: "15px" }}>លេខទូរស័ព្ទ: 023 333 333</p>
                <p style={{ fontFamily: "nokora", fontSize: "15px" }}>អ៊ីម៉ែល:<span style={{ fontFamily: "poppins-regular", fontSize: "15px" }}> servenak@gmail.com</span></p>
                <p style={{ fontFamily: "nokora", fontSize: "15px" }}>បណ្ដាញសង្គម</p>
                <span style={{ marginLeft: "15px", fontSize: "30px", color: "#3b5998" }}>
                  <i class="bi bi-facebook" style={{ href: "#" }}></i>
                </span>
                <span style={{ marginLeft: "15px", fontSize: "30px", color: "#0088cc" }}>
                  <i class="bi bi-telegram"></i>
                </span>
                <span style={{ marginLeft: "15px", fontSize: "30px", color: "#8a3ab9" }}>
                  <i class="bi bi-instagram"></i>
                </span>
              </div>
              <div class="col-md-3 mx-auto mb-4" style={{ marginTop: "85px", textAlign: "center", lineHeight: "30px"}}>
                <h6 style={{ fontFamily: "nokora-bold", fontSize: "22px" }}>អាស័យដ្ឋាន</h6>
                <p style={{ fontFamily: "nokora", fontSize: "15px", textDecoration: "none" }}
                  href="#!"
                  class="text-reset">
                  ផ្ទះ​លេខ ១២ ផ្លូវលេខ ៣២៣ សង្កាត់ បឹងកក់​ II ខណ្ឌ ទួលគោក រាជធានី ភ្នំពេញ ព្រះរាជាណាចក្រកម្ពុជា
                </p>
              </div>
            </div>
          </div>
        </section>
      </footer>
    </div>
  );
}
