/**********************************************************************
 * Development By Group: ServeNak
 * Description: route for business owner dashboard
 **********************************************************************/

import { Switch, Route } from "react-router-dom";
import "../assets/Style/App.css";
import BoSidebar from "../components/sidebar/boSidebar.js";
import BoHome from "../views/dashboard/bo/BoHome.jsx";
import BoHeader from "../components/header/boHeader.js";
import { BoSetting } from "../views/dashboard/bo/BoSetting.jsx";
import PostService from "../views/dashboard/bo/PostService.jsx";
import Match from "../views/dashboard/bo/Match";
import Store from "../views/dashboard/bo/Store";

const bo = ({ match }) => {
  return (
    <div>
      <BoHeader />
      <Switch>
        <Route path={match.url} exact={true} component={BoHome} />
        {/* <Route
          path={`${match.url}/create-service`}
          exact={true}
          component={CreateService}
        /> */}
        <Route
          path={`${match.url}/bo-setting`}
          exact={true}
          component={BoSetting}
        />
        <Route
          path={`${match.url}/match`}
          exact={true}
          component={Match}
        />
        <Route path={`${match.url}/player`} component={PostService} />
        <Route
          path={`${match.url}/store`}
          exact={true}
          component={Store}
        />
      </Switch>
      <BoSidebar />
    </div>
  );
};

export default bo;
