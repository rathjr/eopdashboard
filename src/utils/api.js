import axios from 'axios';
const api = axios.create({
    baseURL: 'https://api.eop.digital/api',
});

export default api;